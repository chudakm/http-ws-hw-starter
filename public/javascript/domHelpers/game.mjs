export const renderRooms = (rooms, onJoin) => {
  rooms.forEach(room => {
    const roomsElement = document.getElementById("rooms-list");

    const roomElement = document.createElement("div");
    roomElement.className = "room";
    roomElement.id = "room" + room.name;

    const connectedCountElement = document.createElement("span");
    connectedCountElement.className = "room-connected-count";
    connectedCountElement.textContent = room.users.length + " user connected";

    const roomNameElement = document.createElement("span");
    roomNameElement.className = "room-name";
    roomNameElement.textContent = room.name;

    const joinBtn = document.createElement("button");
    joinBtn.className = "join-btn";
    joinBtn.id = "join-btn";
    joinBtn.textContent = "Join";
    joinBtn.addEventListener("click", () => onJoin(room));

    roomElement.appendChild(connectedCountElement);
    roomElement.appendChild(roomNameElement);
    roomElement.appendChild(joinBtn);

    roomsElement.appendChild(roomElement);
  });
};

export const removeRooms = () => {
  const roomsElement = document.getElementById("rooms-list");
  if (roomsElement) {
    while (roomsElement.lastChild) {
      roomsElement.removeChild(roomsElement.lastChild);
    }
  }
};

export const changePageToGame = name => {
  const roomNameElement = document.getElementById("room-name");
  roomNameElement.textContent = name;
  hideRoomsPage();
  showGamePage();
};

export const changePageToRooms = () => {
  hideGamePage();
  showRoomsPage();
};

export const showRoomsPage = () => {
  showElement("rooms-page");
};

export const hideRoomsPage = () => {
  hideElement("rooms-page");
};

export const showGamePage = () => {
  showElement("game-page");
};

export const hideGamePage = () => {
  hideElement("game-page");
};

const hideElement = id => {
  const element = document.getElementById(id);
  element.className += " display-none";
};

const showElement = id => {
  const element = document.getElementById(id);
  element.className = element.className.replace("display-none", "");
};

export const removeUsers = () => {
  const usersListElement = document.getElementById("users-list");
  if (usersListElement) {
    while (usersListElement.lastChild) {
      usersListElement.removeChild(usersListElement.lastChild);
    }
  }
};

export const renderUsers = users => {
  users.forEach(user => renderUser(user));
};

const renderUser = user => {
  const username = sessionStorage.getItem("username");

  const userElement = document.createElement("div");
  userElement.className = "user";

  const userInfoElement = document.createElement("div");
  userInfoElement.className = "user-info";

  const readyStatusElement = document.createElement("div");
  readyStatusElement.className = `ready-status ready-status-${user.isReady ? "green" : "red"}`;

  const usernameElement = document.createElement("span");
  usernameElement.className = "user-name";
  usernameElement.textContent = `${user.name} ${user.name === username ? "(you)" : ""}`;

  const userProgressBorderElement = document.createElement("div");
  userProgressBorderElement.className = `user-progress-border`;

  const userProgressBarElement = document.createElement("div");
  userProgressBarElement.className = `user-progress ${user.name} ${user.progress === 100 ? "finished" : ""}`;
  userProgressBarElement.style = `width: ${user.progress}%;background-color: ${user.progress === 100 ? "green" : "greenyellow"}`;

  userProgressBorderElement.appendChild(userProgressBarElement);

  userInfoElement.appendChild(readyStatusElement);
  userInfoElement.appendChild(usernameElement);

  userElement.appendChild(userInfoElement);
  userElement.appendChild(userProgressBorderElement);

  const usersListElement = document.getElementById("users-list");
  usersListElement.appendChild(userElement);
};

export const removeReadyButton = () => {
  document.getElementById("ready-btn")?.remove();
};

export const renderReadyButton = (text, onClick) => {
  const gameAreaElement = document.getElementById("game-area");

  const button = document.createElement("button");
  button.id = "ready-btn";
  button.className = "ready-btn";
  button.textContent = text.toUpperCase();
  button.addEventListener("click", onClick);

  gameAreaElement.appendChild(button);
};

export const removeTimer = () => {
  document.getElementById("timer")?.remove();
};

export const renderTimer = time => {
  const timerElement = document.createElement("span");
  timerElement.id = "timer";
  timerElement.textContent = time;

  const gameAreaElement = document.getElementById("game-area");
  gameAreaElement.appendChild(timerElement);
};

export const removeBackToRoomsButton = () => {
  document.getElementById("quit-room-btn")?.remove();
};

export const renderBackToRoomsButton = () => {
  const roomNameElement = document.getElementById("room-name");
  if (!roomNameElement) return;

  const backToRoomsButton = document.createElement("button");
  backToRoomsButton.id = "quit-room-btn";
  backToRoomsButton.className = "back-to-rooms";
  backToRoomsButton.textContent = "Back To Rooms";

  roomNameElement.parentNode.insertBefore(backToRoomsButton, roomNameElement.nextSibling);
};

export const removeTextContainer = () => {
  document.getElementById("text-container")?.remove();
};

export const renderTextContainer = (text, correctText) => {
  const textContainerElement = document.createElement("div");
  textContainerElement.id = "text-container";

  if (correctText) {
    const correctTextElement = document.createElement("span");
    correctTextElement.className = "correct-text underline-next";
    correctTextElement.textContent = correctText;

    textContainerElement.appendChild(correctTextElement);

    if (!text) {
      correctTextElement.className = correctTextElement.className.replace("underline-next", "");
    }
  }

  const nextTextElement = document.createElement("span");
  nextTextElement.className = "next-text";
  nextTextElement.textContent = text;

  textContainerElement.appendChild(nextTextElement);

  const gameAreaElement = document.getElementById("game-area");
  gameAreaElement.appendChild(textContainerElement);
};

export const updateTimeLeftTimerText = text => {
  const timeLeftElement = document.getElementById("time-left");
  timeLeftElement.textContent = text;
};

export const renderWinnersModal = (users, onClose) => {
  const modalElement = document.createElement("div");
  modalElement.className = "winners-modal";
  modalElement.id = "winners-modal";

  const headerElement = document.createElement("div");
  headerElement.className = "winners-modal-header";

  const headerTitleElement = document.createElement("span");
  headerTitleElement.textContent = "Game Results";

  const headerCloseElement = document.createElement("div");
  headerCloseElement.style = "font-size: 1.2rem; font-weight: 700; cursor: pointer;";
  headerCloseElement.textContent = "X";
  headerCloseElement.id = "quit-results-btn";
  headerCloseElement.addEventListener("click", onClose);

  headerElement.appendChild(headerTitleElement);
  headerElement.appendChild(headerCloseElement);

  const bodyElement = document.createElement("div");
  bodyElement.className = "winner-modal-body";

  users.forEach((user, i) => {
    const userElement = document.createElement("span");
    userElement.className = "winner-modal-user";
    userElement.id = ` place-${i + 1}`;
    userElement.textContent = `${i + 1}. ${user.name}`;

    bodyElement.appendChild(userElement);
  });

  modalElement.appendChild(headerElement);
  modalElement.appendChild(bodyElement);

  const gamePageElement = document.getElementById("game-page");
  gamePageElement.style = "background:rgba(0,0,0,.5);";

  const rootElement = document.getElementById("root");
  rootElement.appendChild(modalElement);
};

export const removeWinnersModal = () => {
  document.getElementById("winners-modal")?.remove();

  const gamePage = document.getElementById("game-page");
  if (gamePage) gamePage.style = "";
};
