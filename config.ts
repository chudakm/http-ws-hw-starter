import path from "path";

export const STATIC_PATH = path.resolve("public");
export const HTML_FILES_PATH = path.resolve(STATIC_PATH, "html");

export const PORT = process.env.PORT || 3002;
