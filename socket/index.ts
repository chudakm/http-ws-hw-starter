import { Server } from "socket.io";
import * as config from "./config";
import game from "./game";

export default (io: Server) => {
  game(io.of("/game"));
};
