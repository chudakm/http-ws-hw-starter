import { Namespace } from "socket.io";
import { SECONDS_FOR_GAME } from "../socket/config";
import { sleep } from "../util/sleep";

export class GameTimers {
  private timers: Map<string, { stopTimer: () => void }> = new Map();

  public async start(io: Namespace, roomName: string) {
    let stop = false;

    const stopTimer = () => {
      stop = true;
    };

    this.timers.set(roomName, { stopTimer });

    for (let i = SECONDS_FOR_GAME; i--; i > 0) {
      if (stop) {
        throw Error("stopped");
      }
      io.to(roomName).emit("UPDATE_TIME_LEFT_TIMER", i);

      await sleep(1000);
    }
  }

  public stop(roomName: string) {
    this.timers.get(roomName)?.stopTimer();
  }
}
